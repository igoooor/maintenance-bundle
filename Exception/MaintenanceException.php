<?php


namespace Iweigel\MaintenanceBundle\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;

class MaintenanceException extends HttpException
{
    public function __construct() {
        parent::__construct(503, "Platform disabled, running updates");
    }
}