<?php


namespace Iweigel\MaintenanceBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DisablePlatformCommand extends Command {

    protected function configure()
    {
        $this
            ->setName('iweigel:maintenance:disable-platform')
            ->setDescription('Disable the platform.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        touch("web/.DISABLED");
        $output->writeln("PLATFORM DISABLED!");
    }
}