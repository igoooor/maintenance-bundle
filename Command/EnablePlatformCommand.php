<?php


namespace Iweigel\MaintenanceBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EnablePlatformCommand extends Command {

    protected function configure()
    {
        $this
            ->setName('iweigel:maintenance:enable-platform')
            ->setDescription('Enable the platform.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        unlink("web/.DISABLED");
        $output->writeln("PLATFORM ENABLED!");
    }
}