<?php


namespace Iweigel\MaintenanceBundle\EventListener;

use Iweigel\MaintenanceBundle\Exception\MaintenanceException;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class PlatformDisabledListener implements EventSubscriberInterface
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (file_exists(".DISABLED") && !$event->getRequest()->attributes->get('exception')) {
            throw new MaintenanceException();
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => [['onKernelRequest']],
        ];
    }
}